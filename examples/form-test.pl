#! /usr/local/bin/perl -w
# Modify above path to point to perl on your system
# which for Linux is typically /usr/bin/perl

# formtest.cgi (formtest.pl) is intended as CGI script
# to test all variables submitted by a web form
# Copyright (C) 1997 by David Efflandt efflandt253@gmail.com
# PO Box 988, Elgin, IL 60121-0988 USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# CGI scripts require read and execute permission for the user or group
# the web server runs as, so typically permissions would be
# 'chmod 755 filename' from shell (see 'man chmod').
#
# If ftp uploaded from DOS|Win, do that as ASCII (not binary)
# so line endings will be correct for system uploaded to.

# HTML header
print "Content-type: text/html\n\n";
print "<html><head><title>Form Test</title></head>\n<body>\n";
print "<h1>Form Test Results</h1>\n";

# Print POST data if any
if ($ENV{'REQUEST_METHOD'} eq "POST") {
  read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
  print "<p><hr>\n<h2>POST Data</h2>\n";
  print "<h3>Raw STDIN:</h3>\n";
  print "<pre>\n$buffer\n</pre>\n";
  &listdata;
} 

# Print GET data (QUERY_STRING) if any
if ($ENV{'QUERY_STRING'}) {
  $buffer = $ENV{'QUERY_STRING'};
  print "<p><hr>\n<h2>GET data</h2>\n";
  print "<h3>Raw QUERY_STRING:</h3>\n";
  print "<pre>\n$buffer\n</pre>\n";
  &listdata;
}

# No Data response
unless ($buffer) {
  print "<p><hr>\n<h2>No Form Data Submitted</h2>\n";
  print "This script will display any form data submitted ";
  print "to it using the GET or POST method.\n";
  exit;
}

print "<p></body></html>\n";

# List the variables
sub listdata {
  print "<h3>Variables:</h3>\n";
  # Split the name-value pairs
  @pairs = split(/&/, $buffer);

  foreach $pair (@pairs) {
    ($name, $value) = split(/=/, $pair);

    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $name =~ tr/+/ /;
    $name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

  # Plain text values
  #  print "<p><b>$name:</b> <pre>$value</pre>\n";

  # HTML values
    print "<p>$name = $value\n";
  }
}
