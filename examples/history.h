/*****
* history.h : public header for XmHTML history routines
*
* This file Version	$Revision: 1.1 $
*
* Creation date:		Sun Aug 30 21:39:34 CEST 1998
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				XmHTML Developers Account
*
* Copyright (C) 1994-1998 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of the XmHTML Widget Library
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* $Source$
*****/
/*****
* ChangeLog 
* $Log$
*
*****/ 

#ifndef _history_h_
#define _history_h_

typedef struct{
	String file;
	int line_no;
	String location;
}HistoryData;

/* Initialize the history routines. Level > 0 : debug mode */
extern void HistoryInitialize(int level);

/* add an item to the history */
extern Boolean HistoryAdd(Widget w, String file, int line_no, String location);

/* remove the history for the given widget */
extern void HistoryRemove(Widget w);

/* remove all history items associated with the given file */
extern Boolean HistoryRemoveRefs(Widget w, String file);

/*****
* History fetching routines.
* All these routines return pointers to static data, so do not free
* the returned data.
*****/

/* get the previous item in the history list */
extern HistoryData *HistoryBack(Widget w);

/* get the current item in the history list */
extern HistoryData *HistoryCurrent(Widget w);

/* get the next item in the history list */
extern HistoryData *HistoryForward(Widget w);

/* verify if a backward move in the history will return valid data */
extern Boolean HistoryVerifyBack(Widget w);

/* verify if a forward move in the history will return valid data */
extern Boolean HistoryVerifyForward(Widget w);

/* Don't add anything after this endif! */
#endif /* _history_h_ */

