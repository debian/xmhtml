/*****
* ImBuffer.h : memory buffer routines.
*
* This file Version	$Revision: 1.1 $
*
* Creation date:		Thu May  8 05:52:19 GMT+0100 1997
* Last modification: 	$Date: 1997/05/28 13:14:13 $
* By:					$Author: newt $
* Current State:		$State: Exp $
*
* Author:				Koen D'Hondt <ripley@xs4all.nl>
*
* Copyright (C) 1994-1997 by Ripley Software Development 
* All Rights Reserved
*
* Permission to use, copy, modify, and distribute this software and its
* documentation for any purpose and without fee is hereby granted, provided
* that the above copyright notice appear in all copies and that both that
* copyright notice and this permission notice appear in supporting
* documentation.  This software is provided "as is" without express or
* implied warranty.
*
*****/
/*****
* $Source: /usr/local/rcs/Newt/XmHTML/RCS/ImBuffer.h,v $
*****/
/*****
* ChangeLog 
* $Log: ImBuffer.h,v $
* Revision 1.1  1997/05/28 13:14:13  newt
* Initial Revision
*
*****/ 

#ifndef _ImBuffer_h_
#define _ImBuffer_h_

#ifndef BYTE_ALREADY_TYPEDEFED
#define BYTE_ALREADY_TYPEDEFED
typedef unsigned char Byte;
#endif /* BYTE_ALREADY_TYPEDEFED */

/*****
* Image memory buffer structure. 
* mimics FILE access, only a lot faster
*****/
typedef struct{
	char *file;			/* name of file */
	Byte *buffer;		/* memory buffer */
	size_t next;		/* current block count */
	size_t size;		/* size of memory buffer */
	int may_free;		/* True when we must free this block */
	Byte type;			/* type of image */
}ImageBuffer;

#define	RewindImageBuffer(IB)	(IB)->next = (size_t)0

#define FreeImageBuffer(IB) { \
	if((IB)->may_free) { \
		free((IB)->file); \
		free((IB)->buffer); \
		free((IB)); \
		(IB) = NULL; \
	} \
}

/* load the given file into new buffer */
extern ImageBuffer *ImageFileToBuffer(char *file);

/* read len chars from ib to buf */
extern size_t ReadOK(ImageBuffer *ib, Byte *buf, int len);

extern size_t GifGetDataBlock(ImageBuffer *ib, Byte *buf);

/* Don't add anything after this endif! */
#endif /* _ImBuffer_h_ */
