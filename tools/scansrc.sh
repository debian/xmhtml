#!/bin/sh
# Collect all function descriptions of each source file in the current
# directory
#
echo "Listing of commented functions found in C source files, "
echo "directory $1."
echo 
echo "Generated at `date` by `whoami`"
echo

filelist=`echo $1/*.c`
nfiles=0
ntotal=0

echo "Files scanned:"
echo "------------------------------------------------------------------"
wc -c $filelist
echo "------------------------------------------------------------------"
echo

for file in $filelist ; do
	nfound=0
	nfiles=`expr $nfiles + 1`
	echo "--------------------------------------------------------------"
	echo "Functions found in file:"
	echo "$file"
	echo "--------------------------------------------------------------"
	echo 
	exec 0<$file
	while read input ; do
		if [ "$input" = "/*****" ] ; then
			read input
			if [ `echo "$input" | grep -c -e "* Name:"` = "1" ] ; then
				echo "/*****"
				echo "$input"
				while read input ; do
					if [ "$input" = "{" ] ; then
						echo
						nfound=`expr $nfound + 1`
						break
					else
						echo "$input"
					fi
				done
			fi
		fi
	done
	if [ "$nfound" = "0" ] ; then
		echo "     (no commented functions found)"
	else
		echo "---> $nfound found"
	fi
	echo
	ntotal=`expr $ntotal + $nfound`
done
echo "------------------------------------------------------------------"
echo "$nfiles files, $ntotal functions found."
