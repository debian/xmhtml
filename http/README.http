This File: README for HTTP.c

-----------------------------------------------------------------------
Note: This file is in pieces until I get around to write a proper set
      of documentation.
-----------------------------------------------------------------------

HTTP is a simple implementation of the HTTP1.0 protocol.

It has been written by Richard Offer (offer@sgi.com) and expanded by
Koen D'Hondt (ripley@xs4all.nl).

Implementation details
----------------------
HTTP uses non-blocking communication with a user-selectable timeout and
retry count.

On systems that do not support the SO_RCVTIMEO setsockopt() option, alarm()
is used to enforce a timeout on connect().

Once a connection has been established, HTTP uses a select() on read() to
ensure the read operation doesn't block.


HTTP Return Values
------------------
Upon return of loadHTTPURL(), the ret field of the HTTPRequest structure
contains a HTTPRequestReturn code indicating success, partial success or
failure to honor the request.

Return values 0-99 are library errors

All return values above 99 are messages returned by the server HTTP is
connecting to.

Classes:
	100-199 : informative messages, can be ignored;
	200-299 : request accepted
	300-399 : non-fatal request errors
	400-499 : fatal request errors
	500-600 : remote server errors

The convenience routine HTTPErrorString(error) returns a string containing
an appropriate error message while the convienence routine
HTTPError(msg, error) prints a user-supplied message (which may be NULL)
together with an appropriate error message.

Value and Meaning of the HTTP Return Values
-------------------------------------------

Value	Name					Meaning
0		HTTPInvalid				invalid request type
1		HTTPBadProtocol			unsupported protocol requested.
								Currently only http is supported
2		HTTPBadHost				hostname could not be resolved
3		HTTPBadURL				badly formed URI in the HTTPRequest
4		HTTPBadLoadType			unsupported data return method requested.
5		HTTPMethodUnsupported	unsupported HTTP method requested. GET, POST
								and HEAD are the only supported (and possible)
								methods
6		HTTPNoSocket			could not create a socket
7		HTTPNoConnection		connect() failed
8		HTTPBadHttp10			unused
9		HTTPCannotCreateFile	library could not create the file requested
								by the HTTPLoadToFile LoadType
10		HTTPConnectTimeout		initial connect() failed, timed out.
11		HTTPTimeout				read() timeout

100		HTTPContinue
101		HTTPSwitchProtocols

200		HTTPSuccess				request succeeded and fully honored
201		HTTPCreated
202		HTTPAccepted
203		HTTPNonAuthoritativeInfo
204		HTTPNoContent
205		HTTPResetContent
206		HTTPPartialContent		request only partially served

300		HTTPMultipleChoices
301		HTTPPermMoved			document permanently moved to a new location
302		HTTPTempMoved			document temporarely moved to a new location
303		HTTPSeeOther
304		HTTPNotModified			document not modified since last request
								(proxy message?)
305		HTTPUseProxy

400		HTTPBadRequest
401		HTTPUnauthorised		access denied, authorization required
402		HTTPPaymentReq			access denied, first you must pay
403		HTTPForbidden			access forbidden
404		HTTPNotFound			requested document not found
405		HTTPMethodNotAllowed
406		HTTPNotAcceptable
407		HTTPProxyAuthReq		proxy authorization required
408		HTTPRequestTimeOut		request timed out
409		HTTPConflict
410		HTTPGone
411		HTTPLengthReq
412		HTTPPreCondFailed
413		HTTPReqEntityTooBig
414		HTTPURITooBig
415		HTTPUnsupportedMediaType

500		HTTPInternalServerError
501		HTTPNotImplemented
502		HTTPBadGateway
503		HTTPServiceUnavailable
504		HTTPGatewayTimeOut
505		HTTPHTTPVersionNotSupported

