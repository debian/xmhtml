##############################################################################
#                                                                            #
# Makefile.org: default XmHTML tools makefile.                               #
# Adjust at will                                                             #
#                                                                            #
##############################################################################
#                                                                            #
# (C)Copyright 1995-1997 Ripley Software Development                         #
# All Rights Reserved                                                        #
#                                                                            #
# This file is part of the XmHTML Widget Library.                            #
#                                                                            #
# This library is free software; you can redistribute it and/or              #
# modify it under the terms of the GNU Library General Public                #
# License as published by the Free Software Foundation; either               #
# version 2 of the License, or (at your option) any later version.           #
#                                                                            #
# This library is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          #
# Library General Public License for more details.                           #
#                                                                            #
# You should have received a copy of the GNU Library General Public          #
# License along with this library; if not, write to the Free                 #
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.         #
##############################################################################

# List of sources
SRCS = GetRepTypes.c WidgetSize.c gifinfo.c ImBuffer.c gif2gzf.c \
	httpget.c miniparse.c mkStrings.c ../lib/common/parse.c ../lib/common/LZWStream.c

# List of object files
OBJS = GetRepTypes.o WidgetSize.o gifinfo.o ImBuffer.o gif2gzf.o \
	httpget.o miniparse.o parse.o LZWStream.o mkStrings.o

# Targets to make
TARGETS = reptype wsize gifinfo HTMLparse gif2gzf httpget mkStrings strings

# rule to create .o files from .c files 
.c.o:
	$(RM) $@
	$(CC) $(INCLUDES) $(CFLAGS) $(CPPFLAGS) -c $<

#
# Special object rules
#

#
# gif2gzf needs to know where zlib lives
#
gif2gzf.o: gif2gzf.c
	$(RM) $@
	$(CC) -c $(INCLUDES) $(CFLAGS) $(CPPFLAGS) $(ZLIBINC) $*.c

#
# LZWStream compiled for standalone usage
#
LZWStream.o: ../lib/common/LZWStream.c
	$(RM) $@
	$(CC) -c $(INCLUDES) $(CFLAGS) $(CPPFLAGS) -DNO_XmHTML ../lib/common/LZWStream.c

#
# XmHTML HTML Parser compiled for standalone usage
#
parse.o: ../lib/common/parse.c
	$(RM) $@
	$(CC) -c $(INCLUDES) $(CFLAGS) $(CPPFLAGS) -DMINIPARSE ../lib/common/parse.c

#
# All Targets
#

all: $(TARGETS)

# targets to build

reptype::  GetRepTypes.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) GetRepTypes.o $(LOADLIBES)

wsize::  WidgetSize.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) WidgetSize.o

gifinfo::  gifinfo.o ImBuffer.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) gifinfo.o ImBuffer.o

httpget::  httpget.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) httpget.o -L../http -lhttp $(DMALLOCLIB)

mkStrings::  mkStrings.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) mkStrings.o

# gif to gzf converter
gif2gzf:: gif2gzf.o LZWStream.o ImBuffer.o
	$(RM) $@ \ 
	$(CC) -o $@ $(LDFLAGS) gif2gzf.o LZWStream.o ImBuffer.o $(ZLIBLIB)

# stand alone parser
HTMLparse:: parse.o miniparse.o
	$(RM) $@
	$(CC) -o $@ $(LDFLAGS) parse.o miniparse.o

depend:: $(SRCS) 
	$(MAKEDEPEND) $(INCLUDES) $(CPPFLAGS) -DMINIPARSE -DNO_XmHTML $(SRCS)

clean::
	$(RM) $(OBJS)  
	$(RM) $(TARGETS)

distclean:: clean 
	$(RM) core *.out *.log make.world *.bak *.last *.auto *.rej *.orig
	#$(CP) Makefile.org Makefile


#Generated headers by mkStrings:
#Generate the resource & warning string tables.
strings: mkStrings strings.lst warnings.lst
	./mkStrings XmHTML -c < strings.lst > ../lib/Motif/strings.c
	./mkStrings XmHTML -h < strings.lst > ../include/XmHTML/HTMLStrings.h
	./mkStrings XmHTML -c -w < warnings.lst > ../lib/Motif/warnings.c
	./mkStrings XmHTML -h -w < warnings.lst > ../include/common/HTMLWarnings.h
	touch $@


#--------------------------------------------------------------------------
# don't delete anything below this line, makedepend depends on it
#--------------------------------------------------------------------------
