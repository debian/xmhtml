/* autosize_html.c -- Demonstrate the autosizing feature of a XmHTML widget */
#include <XmHTML/XmHTML.h>

int
main(int argc, char **argv)
{
	Widget toplevel;
	XtAppContext app;

	toplevel = XtVaAppInitialize(&app, "Demos", NULL, 0,
		&argc, argv, NULL, NULL);

	/* make sure we may resize ourselves */
	XtVaSetValues(toplevel, XmNallowShellResize, True, NULL);

	/* create a XmHTML widget but this time enable autosizing */
	XtVaCreateManagedWidget("html", xmHTMLWidgetClass, toplevel,
		XmNvalue,
/* "<html><body>An AutoSizing XmHTML widget.</body></html>", */
"<html><body>"
"Sat Sep 21 22:00:02 UTC 1996 * Yeltsin had heart attack during Russian elections.   * Doctor says surgery may be too dangerous.   * Ghost of Papandreou hovers over Greek elections.   * Israeli planes hit suspected guerrilla targets in Lebanon.   * Tensions heating up in southern Lebanon.   * India's former prime minister resigns as party chief.   * Coalition parties mull successor.   * Army manuals appear to condone human rights abuse.   * VMI accepts women, ends 157-year school policy.   * Clinton bans same-sex marriages.   * Olympic bomb probe focuses on 12-volt battery.   * Justice Department to probe CIA drug-peddling charges." 
"</body> </html>",
		XmNresizeWidth, True,
		XmNresizeHeight, True,
		XmNmarginWidth, 1,
		XmNmarginHeight, 1,
		NULL);

	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
	return(0);
}
