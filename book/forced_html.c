/* forced_html.c -- Force the display of the vertical scrollbar */
#include <XmHTML/XmHTML.h>

int
main(int argc, char **argv)
{
	Widget toplevel;
	XtAppContext app;

	toplevel = XtVaAppInitialize(&app, "Demos", NULL, 0,
		&argc, argv, NULL, NULL);

	/* create a XmHTML widget but this time we specify a size */
	XtVaCreateManagedWidget("html", xmHTMLWidgetClass, toplevel,
		XmNvalue, "<html><body>A minimally configured "
			"XmHTML widget.</body></html>",
		XmNwidth, 200,
		XmNheight, 75,
		XmNscrollBarDisplayPolicy, XmSTATIC,
		XmNscrollBarPlacement, XmTOP_LEFT,
		XmNmarginWidth, 5,
		XmNmarginHeight, 5,
		NULL);

	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
	return(0);
}
