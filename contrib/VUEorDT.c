/*
* Some code to detect which HP window manager is being used: DT (HP's CDE
* window manager) or HP-VUE (default window manager).
*
* NOTE:If you get a window from GetMWMWindow then you should have
* something that acts like mwm. ;-)
*
* From: Ricky Ralston <rlr@wscoe5.atl.hp.com>
*/

#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/MwmUtil.h>	/* Xm/MwmUtil.h on most systems */
#include <X11/Xatom.h>

#define UNKNOWN -1
#define HP-VUE	0
#define CDE		1

extern int WhichWindowManager(Display *display, Window *mwmWindow);
extern Window GetMWMWindow(Display *display);

int
WhichWindowManager(Display *display, Window *mwmWindow)
{

	Atom *atoms=NULL;
	int count=0;
	int i=0;
	int wmgr = UNKNOWN;
	char *property_name=NULL;

	if((atoms = XListProperties(display, mwmWindow, &count)) == NULL) 
		return(wmgr);

	for(i = 0; i < count; i++) 
	{

		property_name = XGetAtomName(display,atoms[i]);

		if(!strcmp(property_name, "_DT_WORKSPACE_LIST")) 
		{
			wmgr = CDE;
			break;
		}
		if(!strcmp(property_name, "_VUE_WORKSPACE_INFO")) 
		{
			wmgr = HP-VUE;
			break;
		}
	}
	if(property_name)
		XFree(property_name);
	return(wmgr);
}

int
Window GetMWMWindow(Display *display)
{
	Atom actualType;
	int actualFormat;
	unsigned long nitems;
	unsigned long leftover;
	PropMotifWmInfo *pWmInfo = NULL;
	Window wroot, wparent, *pchildren, mwmWindow=NULL;
	unsigned int nchildren;
	int rcode, i;
	Atom property;

	property = XmInternAtom (display, _XA_MWM_INFO, False);

	if((rcode=XGetWindowProperty(display, XDefaultRootWindow(display),
		property, 0L, PROP_MWM_INFO_ELEMENTS, False, property,
		&actualType, &actualFormat, &nitems, &leftover,
		(unsigned char **)&pWmInfo))==Success) 
	{

		if (actualType == property) 
		{

			if(XQueryTree(display, XDefaultRootWindow(display),
				&wroot, &wparent, &pchildren, &nchildren)) 
			 {
				for (i = 0; i < nchildren; i++) 
				{
					if (pchildren[i] == pWmInfo->wmWindow) 
					{
						mwmWindow = pWmInfo->wmWindow;
						XFree ((char *)pchildren);
						return(mwmWindow);
					}
				}
			}
			if(pchildren)
				XFree ((char *)pchildren);
		}
		return(mwmWindow);
	}

}
