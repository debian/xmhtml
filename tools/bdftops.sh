#!/bin/sh
#
# bdf font data
# swidth: scalable width
# dwidth: scalable width

# Font properties
#
# width = fontboundingbox[0]
# height = fontboundingbox[1]
# xoffset = fontboundingbox[2]
# yoffset = fontboundingbox[3]

# ascent = height + y_offset
# descent = -(yoffset)

# min_bounds, max_bounds

# XCharStruct
# width = bbx[0]
# height = bbx[1]
# xoffset = bbx[2]
# yoffset = bbx[3]
#
# lbearing = xoffset
# rbearing = width + xoffset
# width = bbx[0]
# ascent = height + yoffset
# descent = -(yoffset)
